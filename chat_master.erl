%% @doc Master module for the chat system.

-module(chat_master).

-export([start/0, stop/0, start_node/1, stop_node/1, loop/0]).

%% @doc Starts and registers a master process.
start() ->
  Pid = spawn(?MODULE, loop, []),
  erlang:register(?MODULE, Pid).

%% @doc Terminates master process.
stop() ->
  ?MODULE ! stop.

loop() ->
  receive
    {start_node, Src, Nick} when is_pid(Src) ->
      Pid = spawn(chat_node, start, [Nick]),
      Src ! {start_node, Pid},
      loop();
    {stop_node, Pid} when is_pid(Pid) ->
      Pid ! stop,
      loop();
    stop ->
      ok;
    Unknown ->
      io:format("Unknown message received: ~p~n", [Unknown]),
      loop()
  end.

%% @doc Starts chat client process.
-spec(start_node(list()) -> pid()).
start_node(Nick) ->
  ?MODULE ! {start_node, self(), Nick},
  receive
    {start_node, Pid} ->
      Pid
  end.

%% @doc Terminates chat client process.
-spec(stop_node(pid()) -> atom()).
stop_node(Pid) when is_pid(Pid) ->
  ?MODULE ! {stop_node, Pid}.
