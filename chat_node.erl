-module(chat_node).

-export([start/1, history/1, go_online/1, go_offline/1, send_message/3, state/1]).

start(Nick) ->
  Nicks = [{self(), Nick}],
  loop(online, Nicks, []).

loop(State, Nicks, Messages) ->
  receive
    {msg, From, Body} when is_pid(From) ->
      request_nick(Nicks, From),
      loop(State, Nicks, save_message(Messages, {from, From, Body}));
    {send, To, Body} when is_pid(To) ->
      To ! {msg, self(), Body},
      request_nick(Nicks, To),
      loop(State, Nicks, save_message(Messages, {to, To, Body}));
    offline ->
      loop(offline, Nicks, Messages);
    online ->
      loop(online, Nicks, Messages);
    stop ->
      ok;
    {state, Src} when is_pid(Src) ->
      Src ! {state, State},
      loop(State, Nicks, Messages);
    {nick, Src} when is_pid(Src) ->
      Nick = find_nick(Nicks, self()),
      Src ! {nick, self(), Nick},
      loop(State, Nicks, Messages);
    {nick, Pid, Nick} ->
      NewNicks = lists:keystore(Pid, 1, Nicks, {Pid, Nick}),
      loop(State, NewNicks, Messages);
    {history, Src} when is_pid(Src) ->
      Src ! {history, Nicks, Messages},
      loop(State, Nicks, Messages);
    Unknown ->
      io:format("Unknown message received: ~p~n", [Unknown]),
      loop(State, Nicks, Messages)
  end.

save_message(Messages, {Direction, Pid, Body}) ->
  {Mega, Sec, _} = now(),
  Time = Mega * 1000000 + Sec,
  [{Direction, Pid, Body, Time} | Messages].

request_nick(Nicks, Pid) when is_pid(Pid) andalso is_list(Nicks) ->
  case lists:keyfind(Pid, 1, Nicks) of
    false ->
      Pid ! {nick, self()};
    {Pid, _Name} ->
      Nicks
  end.

%% @doc Prints chat client message history.
-spec(history(pid()) -> atom()).
history(Pid) when is_pid(Pid) ->
  case state(Pid) of
    online ->
      Pid ! {history, self()},
      receive
        {history, Nicks, Entries} ->
          print_history(Nicks, Entries)
      after
        1000 ->
          timeout
      end;
    offline ->
      io:format("Client is offline.~n");
    _Undefined ->
      io:format("Client not found.~n")
  end.

print_history(Nicks, Entries) ->
  lists:foreach(fun(Entry) -> print_message(Entry, Nicks) end, lists:reverse(Entries)).

print_message(Entry, Nicks) ->
  {Direction, Pid, Message, Ts} = Entry,
  io:format("[~s] ~s ~s: ~s~n", [ts_to_date_time(Ts), Direction, find_nick(Nicks, Pid), Message]).

ts_to_date_time(Ts) ->
  Mega = Ts div 1000000,
  Sec = Ts rem 1000000,
  {{Y,M,D},{H,I,S}} = calendar:now_to_universal_time({Mega, Sec, 0}),
  lists:concat([Y, "-", M, "-", D, " ", H, ":", I, ":", S]).


find_nick(Nicks, Pid) when is_pid(Pid) andalso is_list(Nicks) ->
  case lists:keyfind(Pid, 1, Nicks) of
    false ->
      "Unknown";
    {Pid, Name} ->
      Name
  end.

%% @doc Turns client into online state.
-spec(go_online(pid()) -> atom()).
go_online(Pid) when is_pid(Pid) ->
  Pid ! online.

%% @doc Turns client into offline state.
-spec(go_offline(pid()) -> atom()).
go_offline(Pid) when is_pid(Pid) ->
  Pid ! offline.

%% @doc Retrieves client's state.
-spec(state(pid()) -> atom()).
state(Pid) when is_pid(Pid) ->
  case process_info(Pid) of
    undefined ->
      undefined;
    _Else ->
      Pid ! {state, self()},
      receive
        {state, State} ->
          State
      after
        1000 ->
          undefined
      end
  end.

%% @doc Sends message from one chat client to another.
-spec(send_message(pid(), pid(), string()) -> atom()).
send_message(From, To, Body) when is_pid(From) andalso is_pid(To) ->
  case state(From) of
    online ->
      case state(To) of
        online ->
          From ! {send, To, Body},
          ok;
        offline ->
          io:format("Recipient is offline.~n");
        undefined ->
          io:format("Recipient is dead.~n")
      end;
    offline ->
      io:format("Sender is offline.~n");
    _Undefined ->
      io:format("Sender is dead.~n")
  end.

